(function(){

let animations = [];
let Animation = function(spritesheet, fps){
    let activeSprite = 0;
    this.count = 0;
    this.step = (systemFps) => {
        if(!spritesheet.loaded) return;
        this.count = (this.count + fps / systemFps) % spritesheet.sprites.length;
        if(Math.floor(this.count) != activeSprite)
            activeSprite = Math.floor(this.count);
    };
    this.getSprite = () => {
        if(!spritesheet.loaded) return null;
        return spritesheet.sprites[activeSprite];
    };

    animations.push(this);
};
Animation.Update = (systemFps) => {
    for(let animation of animations){
        animation.step(systemFps);
    }
};


if(typeof window === "object") window.Animation = Animation;
if(typeof module === "object") module.exports = Animation;

}())
