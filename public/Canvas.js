(function(){

/*** This class is used to create an HTMLCanvasElement
 * that can be drawn on and translated easily ***/

let Canvas = new (function(){
    //creates an HTMLCanvasElement with context
    let canvas = document.createElement('canvas');
    let ctx = canvas.getContext('2d');
    this.ctx = ctx;

    //assigns with and height of the canvas to the dimensions of the window
    canvas.width = window.innerWidth;
    canvas.height = window.innerHeight;

    //inserts canvas into the document
    document.body.insertBefore(canvas, null);


    this.background = "#fff";

    //private variables that track position of canvas translation
    let x = 0;
    let y = 0;

    let minX = null;
    let minY = null;
    let maxX = null;
    let maxY = null;

    //returns private variables that track position of canvas translation
    this.getX = () => {
        return x;
    };
    this.getY = () => {
        return y;
    };
    this.getMinX = () => {
        return minX;
    };
    this.getMinY = () => {
        return minY;
    };
    this.getMaxX = () => {
        return maxX;
    };
    this.getMaxY = () => {
        return maxY;
    };

    //returns width of HTMLCanvasElement
    this.getWidth = () => {
        return canvas.width;
    };
    this.getHeight = () => {
        return canvas.height;
    };

    //translates context on x or y axis
    this.setX = (_x) => {
        if(_x < minX && minX !== null) _x = minX;
        if(_x > maxX && maxX !== null) _x = maxX;
        let diff = x - _x;
        x = _x;
        
        ctx.translate(diff, 0);
    };
    this.setY = (_y) => {
        if(_y < minY && minY !== null) _y = minY;
        if(_y > maxY && maxY !== null) _y = maxY;
        let diff = y - _y;
        y = _y;
        
        ctx.translate(0, diff);
    };

    //translates context on x and y axis
    this.setPos = (_x, _y) => { 
        this.setX(_x);
        this.setY(_y);
        // let diffX = _x - x;
        // x = _x;
        // let diffY = _y - y;
        // y = _y;
        
        // ctx.translate(diffX, diffY);
    };

    this.setMinX = (_minX) => {
        minX = _minX;
    };
    this.setMinY = (_minY) => {
        minY = _minY;
    };
    this.setMaxX = (_maxX) => {
        maxX = _maxX;
    };
    this.setMaxY = (_maxY) => {
        maxY = _maxY;
    };

    //shortcut to clear entire visible canvas
    this.clear = () => {
        ctx.clearRect(x, y, canvas.width, canvas.height);
        ctx.save();
        ctx.fillStyle = this.background;
        ctx.fillRect(x, y, canvas.width, canvas.height);
        ctx.restore();
        return {msg : "cleared successfully", w: canvas.width, h: canvas.height};
    };
})();

if(typeof window === "object") window.Canvas = Canvas;
if(typeof module === "object") module.exports = Canvas;

console.log(Canvas);
})();
