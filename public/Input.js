let Input = {
    inputs : {
        axis : {},
        buttons : {}
    },

    GetAxis : (name) => {
        let axis = Input.inputs.axis[name];
        let value = 0;
        if(axis.positive) value += 1;
        if(axis.negative) value -= 1;
        return value;
    },
    GetButton : (name) => {
        let button = Input.inputs.buttons[name];
        return button.isPressed;
    },
    GetButtonDown : (name) => {
        let button = Input.inputs.buttons[name];
        return button.isDown;
    },
    GetButtonUp : (name) => {
        let button = Input.inputs.buttons[name];
        return button.isUp;
    },

    clearButtonTemps : () => {
        for(let button of Object.values(Input.inputs.buttons)){
            button.isDown = false;
            button.isUp = false;
        }
    }
};

let updateInput = () => {
    Input.clearButtonTemps(); 
};

Input.Axis = function(name, posKeys, negKeys){
    this.value = 0;
    this.keys = {};
    this.positive = false;
    this.negative = false;
    this.keys.positive = posKeys || [];
    this.keys.negative = negKeys || [];

    Input.inputs.axis[name] = this;
};

Input.Button = function(name, keys){
    this.isPressed = false;
    this.isDown = false;
    this.isUp = false;
    this.keys = keys || [];
    
    Input.inputs.buttons[name] = this;
}
let keyDown = (e) => {
    for(let axis of Object.values(Input.inputs.axis)){
        for(let key of axis.keys.positive){
            if(e.key == key) axis.positive = true;
        }
        for(let key of axis.keys.negative){
            if(e.key == key) axis.negative = true;
        }
    }
    for(let button of Object.values(Input.inputs.buttons)){
        for(let key of button.keys) if(e.key == key){
            if(!button.isPressed) button.isDown = true;
            button.isPressed = true;
        }
    }
};
let keyUp = (e) => {
    for(let axis of Object.values(Input.inputs.axis)){
        for(let key of axis.keys.positive){
            if(e.key == key) axis.positive = false;
        }
        for(let key of axis.keys.negative){
            if(e.key == key) axis.negative = false;
        }
    }
    for(let button of Object.values(Input.inputs.buttons)){
        for(let key of button.keys) if(e.key == key){
            button.isPressed = false;
            button.isUp = true;
        }
    }
};

document.addEventListener('keydown', keyDown);
document.addEventListener('keyup', keyUp);
