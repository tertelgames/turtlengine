(function(){

/*** This class if used to create a layer of visible/physical 
 * tiles from a Tiled tilemap ***/


//basic async method to retreive json file
function ajax_get(url, callback) {
    let xmlhttp = new XMLHttpRequest();
    xmlhttp.onreadystatechange = function() {
        let data;
        if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
            //console.log('responseText:' + xmlhttp.responseText);
            try {
                data = JSON.parse(xmlhttp.responseText);
            } catch(err) {
                console.log(err.message + " in " + xmlhttp.responseText);
                return;
            }
            callback(data);
        }
    };
 
    xmlhttp.open("GET", url, true);
    xmlhttp.send();
}

let getLeastRects = (layer) => {
    //will hold complete rectangle data
    let rects = [];

    //keeps track of tiles that are included in rectangles
    let checkedTiles = {};
    
    //loops through tiles
    for(let tileI in layer.data){

        //assigns working tile
        tile = layer.data[tileI];
        //if tile is blank or checked, skip tile
        if(tile == 0) continue;
        if(checkedTiles[tileI]) continue;


        //will hold rectangle checked width first
        let rect = {x : 0, y : 0};

        //x and y position of current subtile (offset)
        let tileX = tileI;
        let tileY = tileI;

        //gets width of working rectangle, assigns rect.x
        for(tileX = tileI; layer.data[tileX] != 0; tileX++);
        // tileX--;
        rect.x = tileX - Number(tileI);
        //checks for full rows below, assigns rect.y
        let fullRow = true;
        for(tileY = Number(tileI); fullRow; tileY += layer.width){
            rect.y += 1;
            for(tileX = tileY; tileX < rect.x + tileI; tileX++){
                if(layer.data[tileX] == 0) fullRow = false;
            }
        }

        rects.push({
            x : tileI % layer.width,
            y : Math.floor(tileI / layer.width),
            width : rect.x,
            height : rect.y,
        });
        for(let x = Number(tileI); x < Number(tileI) + rect.x; x++){
            for(let y = 0; y < rect.y * layer.width; y += layer.width){
                checkedTiles[x + y] = true;
            }
        }
    }
    return rects;
};

let tilemaps = [];
//takes parameters of image source and url to json file
let Tilemap = function(imagesrc, dataurl){
    //will store HTMLCanvasElement of tilemap image
    let img = null;

    //loads HTMLImageElement of tilemap image
    let image = new Image();
    //draws HTMLImageElement to HTMLCanvasElement for more efficient drawing
    image.onload = () => {
        img = document.createElement('canvas');
        img.width = image.width;
        img.height = image.height;
        imgctx = img.getContext('2d');

        imgctx.drawImage(image, 0, 0);
    };
    image.src = imagesrc;

    //will store JSON map of tilemap data
    this.data = null;
    //retreives JSON map
    ajax_get(dataurl, (_data) => {
        this.data = _data;
        for(let layer of _data.layers){
            let rects = getLeastRects(layer);
            for(let rect of rects){
                for(let prop of Object.keys(rect)){
                    rect[prop] *= _data.tilewidth;
                }
                let entity = new Entity(
                        rect.x, rect.y, 
                        rect.width, rect.height
                );
                entity.tag = layer.properties.tag;
                entity.visible = false;
                if(layer.properties.type == "static" ||
                        layer.properties.type == "trigger")
                    entity.static = true;
                if(layer.properties.type == "trigger")
                    entity.trigger = true;
            }
        }
    });

    //draws tilemap if image is loaded
    this.draw = () => {
        //if not loaded, do not attempt draw
        if(!img) return;
        //draw Image only in viewport to avoid excessive calculations
        Canvas.ctx.drawImage(
            img,
            Canvas.getX(), Canvas.getY(), 
            Canvas.getWidth(), Canvas.getHeight(),
            Canvas.getX(), Canvas.getY(), 
            Canvas.getWidth(), Canvas.getHeight()
        );
    };

    //add this instance to the private array of existing tilemaps
    tilemaps.push(this);
};
Tilemap.draw = () => {
    //loop through all instances of tilemaps and call their draw method
    for(let tilemap of tilemaps) tilemap.draw();
};


if(typeof window === "object") window.Tilemap = Tilemap;
if(typeof module === "object") module.exports = Tilemap;

}())
