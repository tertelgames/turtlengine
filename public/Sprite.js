(function(){


//assigns context of Canvas class to a shorter private variable
let ctx = Canvas.ctx;

//takes parameters of image source, optional: x, y, width, height
function Sprite(src, x, y, width, height){
    //holds HTMLCanvasElement for efficient drawing
    let img = document.createElement('canvas');
    //context of HTMLCanvasElement
    let img_ctx = img.getContext('2d');
    //potential reversed version of image
    let revImg = null;

    //will hold dimensions of image; 0 by default
    this.width = 0;
    this.height = 0;

    //boolean will be set true when image is ready for drawing; default false
    let loaded = false;
    //will create reversed version of image on load if set true; default false
    let makeRevOnLoad = false;

    //assigns this instance to a local variable for async scoping
    let self = this;
    
    //loads HTMLImageElement and draws to HTMLCanvasElement async
    let image = new Image();
    image.onload = () => {
        x = x || 0;
        y = y || 0;
        //sets width and height properties
        self.width = img.width = width || image.width;
        self.height = img.height = height || image.height;
        
        img_ctx.drawImage(
            image, 
            x, y, img.width, img.height, 
            0, 0, img.width, img.height
        );
        //sets loaded flag to true
        loaded = true;
        //makes reveresed image if makeRevOnLoad is true
        if(makeRevOnLoad) this.makeRev();
    };
    image.src = src;

    //will make reversed image immediately or async on load
    this.makeRev = () => {
        if(revImg) return;
        if(!loaded){
            makeRevOnLoad = true;
            return;
        }
        revImg = document.createElement('canvas');
        revImg.width = self.width;
        revImg.height = self.height;
        revImg_ctx = revImg.getContext('2d');
        revImg_ctx.translate(img.width, 0);
        revImg_ctx.scale(-1, 1);
        revImg_ctx.drawImage(
            img, 0, 0
        );
    };

    //returns HTMLCanvasElement or reversed version
    this.getImg = (reversed) => {
        if(reversed){
            if(revImg) return revImg;
            return false;
        }
        return img;
    };
};

//draws given sprite at x, y coordinates; optional: width, height
Sprite.draw = (sprite, x, y, width, height) => {
    x = Math.round(x);
    y = Math.round(y);
    width = Math.round(width || sprite.width);
    height = Math.round(height || sprite.height);
    ctx.drawImage(sprite.getImg(), x, y, width, height);
};
//draws reversed version of sprite
Sprite.drawRev = (sprite, x, y, width, height) => {
    if(!sprite.getImg(true)){
        sprite.makeRev();
        return;
    }
    x = Math.round(x);
    y = Math.round(y);
    width = Math.round(width || sprite.width);
    height = Math.round(height || sprite.height);
    ctx.drawImage(sprite.getImg(true), x, y, width, height);
};


// Sprite.animate = (sprites, fps) => {
//     let sprite;
//     let i = 0;

//     window.setInterval(() => {
//         i = (i + 1) % sprites.length;
//         sprite = sprites[i];
//     }, 1000 / fps);

//     return sprite;
// }
// Sprite.ctx = ctx;

if(typeof window === "object") window.Sprite = Sprite;
if(typeof module === "object") module.exports = Sprite;

}())
