(function(){


let Update, FixedUpdate;

let fps = 50;


let gravity = 9.8 * 8 / fps;

let getGrav = () => {
    return gravity;
};
let setGrav = (val) => {
    gravity = val;
};


let running;
let loop = () => {
    Entity.updateEntities(gravity);
    Animation.Update(fps);
    if(typeof FixedUpdate === "function") FixedUpdate(fps);
    updateInput();
    run();
};
let run = () => {
    if(running) setTimeout(loop, 1000 / fps);
};
let render = () => {
    if(running) requestAnimationFrame(render);
    Canvas.clear();
    Tilemap.draw();
    Entity.renderEntities();
    if(typeof Update === "function") Update();
};
let stop = () => {
    running = false;
};
let start = (callback) => {
    running = true;
    if(callback) callback();
    run();
    render();
};

let on = (name, callback) => {
    if(name == "Update") Update = callback;
    if(name == "FixedUpdate") FixedUpdate = callback;
};


new Input.Axis("horizontal", ["d", "ArrowRight"], ["a", "ArrowLeft"]);
new Input.Axis("vertical", ["w", "ArrowUp"], ["s", "ArrowDown"]);
new Input.Button("fire1", [" "]);

let myExports = {
    start,
    stop,
    on,
    getGrav,
    setGrav
};

if(typeof window === "object") window.Engine = myExports;
if(typeof module === "object") module.exports = myExports;


}());
