(function(){

Spritesheet = function(src, tileX, tileY, tileCount, options = {}){
    this.sprites = [];
    this.loaded = false;

    let self = this;

    let image = new Image();
    image.onload = () => {
        let tileW = options.tileW || image.width / tileX; 
        let tileH = options.tileH || image.height / tileY;
        tileCount = tileCount || tileX * tileY;
        for(let y = 0; y < tileH * tileH; y += tileH){
            for(let x = 0; x < tileW * tileX; x += tileW){
                self.sprites.push(new Sprite(
                    src, x, y, tileW, tileH
                ));
                self.sprites[self.sprites.length - 1].makeRev();
                if(self.sprites.length >= tileCount){
                    self.loaded = true;
                    if(self.onload) self.onload();
                    return;
                }
            }
        }
    };
    image.src = src;
};

if(typeof window === "object")  window.Spritesheet = Spritesheet;
if(typeof module === "object")  module.Spritesheet = Spritesheet;

}())
