(function(){


let ctx = Canvas.ctx;

let Entity = function(x, y, width, height, options = {}){
    this.x = x;
    this.y = y;
    this.width = width;
    this.height = height;

    this.static = options.static || false;
    this.trigger = options.trigger || false;
    this.visible = options.visible === false ? options.visible : true;
    this.id = options.id || "";
    this.tag = options.tag || "";
    this.type = options.type || "";
    this.gravity = options.gravity || 1;
    this.sprite = options.animation ? null : options.sprite;
    this.spriteMode = options.spriteMode || "top-left";
    this.spriteReverse = options.spriteReverse || false;
    this.animation = options.animation;

    this.velocity = {
        x : 0,
        y : 0
    };

    this.colInfo = {
        lastX : x,
        lastY : y
    };

    this.getCenter = () => {
        return {
            x : this.x + this.width / 2,
            y : this.y + this.height / 2
        };
    };

    Entity.entities.push(this);
};

Entity.entities = [];

Entity.detectCollisions = () => {
    for(let entity1 of Entity.entities){
        if(entity1.static) continue;
        for(let entity2 of Entity.entities){
            if(entity2 == entity1) continue;
            if (entity1.x < entity2.x + entity2.width &&
                entity1.x + entity1.width > entity2.x &&
                entity1.y < entity2.y + entity2.height &&
                entity1.y + entity1.height > entity2.y) {
                let collision = Entity.handleCollision(entity1, entity2);
                if(entity1.onCollision) entity1.onCollision(collision);
            }
        }
    }
};

Entity.getCollisionSide = (entity1, entity2) => {
    let sideHit;

    let last1 = {
        x: entity1.colInfo.lastX,
        y: entity1.colInfo.lastY
    };
    let last2 = {
        x: entity2.colInfo.lastX,
        y: entity2.colInfo.lastY
    };
    if(last1.x < last2.x + entity2.width &&
            last1.x + entity1.width > last2.x){
        if(entity1.y + (entity1.height / 2) >
           entity2.y + (entity2.height / 2)){
            sideHit = "bottom";
        }
        else{
            sideHit = "top";
        }
    }
    if(last1.y < last2.y + entity2.height &&
            last1.y + entity1.height > last2.y){
        if(!entity2.trigger && !entity1.trigger) 
        if(entity1.x + entity1.width / 2 >
           entity2.x + entity2.width / 2){
            sideHit = "right";
        }
        else{
            sideHit = "left";
        }
    }
    return sideHit;
};

Entity.handleCollision = (entity1, entity2) => {
    let sideHit = Entity.getCollisionSide(entity1, entity2);
    if(!entity1.trigger && !entity2.trigger){
        switch(Entity.getCollisionSide(entity1, entity2)){
            case "bottom":
                entity1.y = entity2.y + entity2.height;
                entity1.velocity.y = 0;
                break;
            case "top":
                entity1.y = entity2.y - entity1.height;
                entity1.velocity.y = 0;
                break;
            case "right":
                entity1.x = entity2.x + entity2.width;
                entity1.velocity.x = 0;
                break;
            case "left":
                entity1.x = entity2.x - entity1.width;
                entity1.velocity.x = 0;
                break;
        }
    }

    return {
        self : entity1,
        other : entity2,
        side : sideHit
    };
};

Entity.updateEntities = (gravity) => {
    for(let entity of Entity.entities){
        if(entity.static) continue;
        entity.velocity.y += gravity * entity.gravity;
        entity.colInfo.lastX = entity.x;
        entity.colInfo.lastY = entity.y;
        entity.x += entity.velocity.x;
        entity.y += entity.velocity.y;
    }
    Entity.detectCollisions();
};

Entity.renderEntities = () => {
    for(let entity of Entity.entities){
        if(!entity.visible) continue;
        if(entity.animation) entity.sprite = entity.animation.getSprite();
        if(!entity.sprite){
            ctx.fillRect(entity.x, entity.y, entity.width, entity.height);
            continue;
        }
        if(entity.stretchSprite){
            Sprite["draw" + (entity.spriteReverse ? "Rev" : "")](entity.sprite, 
              entity.x, entity.y, entity.width, entity.height);
            continue;
        }
        switch(entity.spriteMode){
            case "top-left":
                Sprite["draw" + (entity.spriteReverse ? "Rev" : "")](entity.sprite, entity.x, entity.y);
                break;
            case "top-right":
                Sprite["draw" + (entity.spriteReverse ? "Rev" : "")](
                    entity.sprite, 
                    entity.x + entity.width - entity.sprite.width, 
                    entity.y
                );
                break;
            case "bottom-left":
                Sprite["draw" + (entity.spriteReverse ? "Rev" : "")](
                    entity.sprite,
                    entity.x,
                    entity.y + entity.height - entity.sprite.height
                );
                break;
            case "bottom-right":
                Sprite["draw" + (entity.spriteReverse ? "Rev" : "")](
                    entity.sprite,
                    entity.x + entity.width - entity.sprite.width,
                    entity.y + entity.height - entity.sprite.height
                );
                break;
            case "center":
                Sprite["draw" + (entity.spriteReverse ? "Rev" : "")](
                    entity.sprite,
                    entity.x + entity.width / 2 - entity.sprite.width / 2,
                    entity.y + entity.height / 2 - entity.sprite.height / 2,
                );
                break;
            case "stretch":
                Sprite["draw" + (entity.spriteReverse ? "Rev" : "")](entity.sprite, 
                    entity.x, entity.y, entity.width, entity.height);
                break;
            default:
                console.error("Sprite is assigned, but no valid sprite mode");
                break;
        }
    }
};

if(typeof window === "object") window.Entity = Entity;
if(typeof module === "object") module.exports = Entity;


}())
