(function(){


Engine.setGrav(Engine.getGrav() * 1.1);


Canvas.background = "#9af";

Canvas.setMinX(0);
Canvas.setMinY(0);

console.log(Canvas.getMinX());

Engine.start(() => {
    console.log("This is the start function");
});

new Input.Button("jump", ["ArrowUp", "w"]);

new Tilemap('tilemaps/level2.png', 'tilemaps/level2.json');

let runAnimation = new Animation(
    new Spritesheet('images/littlemanrun.png', 3, 2, 4, {reverse : true}), 
    10
);
let restAnimation = new Animation(
    new Spritesheet('images/littlemanrest.png', 3, 2, 5, {reverse : true}), 
    6
);
let jumpUpAnimation = new Animation(
    new Spritesheet('images/littlemanjumpup.png', 1, 1),
    0
);
let jumpDownAnimation = new Animation(
    new Spritesheet('images/littlemanjumpdown.png', 1, 1),
    0
);


let player = new Entity(50, 10, 22, 54, {
    id : "player", 
    animation : restAnimation, 
    spriteMode : "center"
});

// new Entity(0, 500, 800, 64, {static : true, tag : "ground"});

let speed = 8;
let jump = 25;

let canJump = false;

let running = false;

Engine.on("FixedUpdate", () => {
    player.velocity.x = Input.GetAxis("horizontal") * speed;
    if(Input.GetButtonDown("jump") && canJump){
        player.velocity.y = -jump;
        canJump = false;
    }


    if(Input.GetAxis("horizontal") == 0) player.animation = restAnimation;
    else {
        if(Input.GetAxis("horizontal") < 0) player.spriteReverse = true;
        else player.spriteReverse = false;
        player.animation = runAnimation;
    }
    if(player.velocity.y != 0){
        if(player.velocity.y < 5) player.animation = jumpUpAnimation;
        if(player.velocity.y > 5) player.animation = jumpDownAnimation;
    }

    if(player.getCenter().x > Canvas.getX() + window.innerWidth * 0.75)
        Canvas.setX(Math.round(player.getCenter().x - window.innerWidth * 0.75));
    if(player.getCenter().x < Canvas.getX() + window.innerWidth * 0.25)
        Canvas.setX(Math.round(player.getCenter().x - window.innerWidth * 0.25));
    if(player.getCenter().y > Canvas.getY() + window.innerHeight * 0.6)
        Canvas.setY(Math.round(player.getCenter().y - window.innerHeight * 0.6));
    if(player.getCenter().y < Canvas.getY() + window.innerHeight * 0.4)
        Canvas.setY(Math.round(player.getCenter().y - window.innerHeight * 0.4));
});

player.onCollision = (collision) => {
    if(collision.other.tag == "ground" && collision.side == "top"){
        canJump = true;
    }
    if(collision.other.tag == "coin") console.log("Coin collect");
};


}());
